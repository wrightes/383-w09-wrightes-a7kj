<!DOCTYPE html>

<html lang="en">
	
	<?php
	/*
	Evan Wright, Homework09, 7 April 2019
	*/
   session_start(); //create a session
   //initialize number of visits session variable
   $numVisits = 0;
   if (isset($_SESSION['num'])) {
      $_SESSION['num']++;
      $numVisits = $_SESSION['num'];
   } else {
      $numVisits = 0;
      $_SESSION['num'] = 0;
   }
	//see if the cmd get variable is passed into the program.
   $cmd = "";
   if (isset($_GET['cmd'])) {
      $cmd =htmlspecialchars($_GET['cmd']);
      if ($cmd != "page1" && $cmd != "page2" && $cmd != "page3") {
         $cmd = "";
      }
   }
	//YOU WILL PUT YOUR FORM HANDLING CODE HERE
	$named = false;
	if(isset($_GET['user'])) {
		$_SESSION['user'] = htmlspecialchars($_GET['user']);
		$_SESSION['named'] = true;
	} else if(isset($_SESSION['named'])){
		$named = true;
	} else {
		$_SESSION['named'] = false;
	}
	
	?>

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		
		<title>Homework 09</title>
	</head>
	
	<body>
		<div class="page-header bg-light p-2">
			<h3>Homework09 - wrightes</h3>
		</div>
		
		<div class="row mt-3"> 
		<div class="col-md-2 ml-md-4">
		<nav>
			<ul class="nav flex-column">
				<li class="nav-item">
					<a class="nav-link" href="index.php?cmd=page1">Random Numbers</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php?cmd=page2">Images</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php?cmd=page3">Input Form</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php">Home</a>
				</li>
			</ul>
		</nav>
		
		<div class="bg-secondary text-white rounded p-2 mb-2"><?php echo($numVisits); ?></div>
		
		<?php
			if($_SESSION['named']) { ?>
				<div class="bg-primary text-white rounded p-2"><?php echo($_SESSION['user']); ?></div> <?php
			}	
		?>
		
		</div>
		
		<div class="col-md-8">
			<?php
				if ($cmd=="page1"){
			?>
				<h2>Random</h2> 
				<table class="table">
				<?php 
					for($x = 1; $x <= 20; $x++) { ?>
						<tr>
						<?php
							for($a = 1; $a <= 5; $a++) { ?>
								<td style="color:#<?php echo(substr(str_shuffle('ABCDEF0123456789'), 0, 6));?>">
									<?php 
										echo(mt_rand(10000,99999));
									?>
								</td>
					<?php
							} ?>
						</tr>
					<?php
					} ?>
				</table>
			<?php

				} else if ($cmd == "page2") {
			?> 
				<div class="container">
					<img class="img-fluid rounded" src="images/<?php echo(mt_rand(1, 3)); ?>.jpg" alt="random photo">
				</div>
			<?php
				} else if ($cmd == "page3") {
			?> 
				<h2>Form</h2>
				<form>
					Username<br>
					<input class="form-control" type="text" name="user"><br>
					<button type="submit" class="btn btn-primary mb-2">Submit</button>
				</form>
			<?php
				} else {
			?> 
				<h2>Home</h2>
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th scope="col">Variable</th>
							<th scope="col">Value</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>HTTP_HOST</th>
							<td><?php 
									echo $_SERVER['HTTP_HOST']
								?></td>
						</tr>
						<tr>
							<th>HTTP_USER_AGENT</th>
							<td><?php 
									echo $_SERVER['HTTP_USER_AGENT']
								?></td>
						</tr>
						<tr>
							<th>REMOTE_ADDR</th>
							<td><?php 
									echo $_SERVER['REMOTE_ADDR']
								?></td>
						</tr>
						<tr>
							<th>SERVER_SOFTWARE</th>
							<td><?php 
									echo $_SERVER['SERVER_SOFTWARE']
								?></td>
						</tr>
						<tr>
							<th>REQUEST_SCHEME</th>
							<td><?php 
									echo $_SERVER['REQUEST_SCHEME']
								?></td>
						</tr>
					</tbody>
				</table>
			<?php
				}
			?>
		</div> </div>
	</body>

</html>